import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ value, clickHandler }) => (
  <button
    className="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-6 rounded-lg"
    onClick={clickHandler}
    type="button"
  >
    {value}
  </button>
);

Button.defaultProps = {
  value: '',
};

Button.propTypes = {
  value: PropTypes.string,
  clickHandler: PropTypes.func.isRequired,
};

export default Button;
