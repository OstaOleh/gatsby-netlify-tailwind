import { graphql, useStaticQuery } from 'gatsby';

const useIndexQuery = () => {
  const data = useStaticQuery(
    graphql`
     query IndexPageTemplate {
        markdownRemark(frontmatter: { templateKey: { eq: "IndexPage" } }) {
          frontmatter {
            title
            image {
              childImageSharp {
                fluid(maxWidth: 2048, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            heading
            subheading
            mainpitch {
              title
              description
            }
            description
          }
        }
      }
    `,
  );
  const { frontmatter } = data.markdownRemark;
  return frontmatter;
};

export default useIndexQuery;
