import React from 'react';
// Components
import IndexPageTemplate from './IndexPageTemplate';
import TemplateWrapper from '../../layouts/MainLayout';
// Hooks
import useIndexQuery from './useIndexQuery';

const IndexPage = () => {
  const data = useIndexQuery();

  return (
    <TemplateWrapper>
      <IndexPageTemplate
        image={data.image}
        title={data.title}
        heading={data.heading}
        subheading={data.subheading}
        mainpitch={data.mainpitch}
        description={data.description}
      />
    </TemplateWrapper>
  );
};

export default IndexPage;
