import React, {
  useCallback, useState, useMemo, useEffect,
} from 'react';
// Components
import NavbarTemplate from './HeaderTemplate';

const Navbar = () => {
  const [active, setActive] = useState(false);
  const [navBarActiveClass, setNavBarActiveClass] = useState('');
  useEffect(() => {
    setNavBarActiveClass(active ? 'is-active' : '');
  }, [active]);

  const toggleHamburger = useCallback(() => {
    setActive(!active);
  }, [active]);

  return (
    useMemo(() => (
      <NavbarTemplate navBarActiveClass={navBarActiveClass} toggleHamburger={toggleHamburger} />
    ), [navBarActiveClass, toggleHamburger])
  );
};

export default Navbar;
