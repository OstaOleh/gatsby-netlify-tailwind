/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
// import PropTypes from 'prop-types';
import { Link } from 'gatsby';
// Assets
import logo from '../../../assets/images/logo.svg';

const NavbarTemplate = (
  // { navBarActiveClass, toggleHamburger }
) => (
  <header className="text-gray-100 bg-gray-900 body-font shadow w-full">
    <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
      <nav className="flex lg:w-2/5 flex-wrap items-center text-base md:ml-auto">
        <a
          className="mr-5 hover:text-gray-900 cursor-pointer border-b border-transparent hover:border-indigo-600"
        >
          About
        </a>
        <a
          className="mr-5 hover:text-gray-900 cursor-pointer border-b border-transparent hover:border-indigo-600"
        >
          Products
        </a>
        <a className="mr-5 hover:text-gray-900 cursor-pointer border-b border-transparent hover:border-indigo-600">
          Investor
          Relations
        </a>
        <a
          className="hover:text-gray-900 cursor-pointer border-b border-transparent hover:border-indigo-600"
        >
          Contact
        </a>
      </nav>
      <Link
        to="/"
        class="flex order-first lg:order-none lg:w-1/5 title-font font-medium items-center lg:items-center lg:justify-center mb-4 md:mb-0"
      >
        <img src={logo} alt="Kaldi" style={{ height: '40px', marginBottom: '10px', marginTop: '10px' }} />
      </Link>
    </div>
  </header>
  // <nav
  //   className="navbar is-transparent"
  //   role="navigation"
  //   aria-label="main-navigation"
  // >
  //   <div className="container">
  //     <div className="navbar-brand">
  //       <Link to="/" className="navbar-item" title="Logo">
  //         <img src={logo} alt="Kaldi" style={{ width: '88px' }} />
  //       </Link>
  //       {/* Hamburger menu */}
  //       <button
  //         type="button"
  //         className={`navbar-burger burger ${navBarActiveClass}`}
  //         data-target="navMenu"
  //         onClick={toggleHamburger}
  //       >
  //         <span />
  //         <span />
  //         <span />
  //       </button>
  //     </div>
  //     <div
  //       id="navMenu"
  //       className={`navbar-menu ${navBarActiveClass}`}
  //     >
  //       <div className="navbar-start has-text-centered">
  //         <Link className="navbar-item" to="/about">
  //           About
  //         </Link>
  //       </div>
  //     </div>
  //   </div>
  // </nav>
);

NavbarTemplate.propTypes = {
  // navBarActiveClass: PropTypes.string.isRequired,
  // toggleHamburger: PropTypes.func.isRequired,
};

export default NavbarTemplate;
