import React, { useMemo } from 'react';
// Components
import FooterTemplate from './FooterTemplate';

const Footer = () => (
  useMemo(() => <FooterTemplate />, [])
);

export default Footer;
