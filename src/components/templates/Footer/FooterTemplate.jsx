/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Link } from 'gatsby';

const FooterTemplate = () => (
  <footer className="bg-gray-800 pt-10 sm:mt-10 pt-10 flex">
    <div className="max-w-6xl m-auto text-gray-800 flex flex-wrap justify-left">
      <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">
        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
          Getting Started
        </div>

        <Link to="/" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
          Home
        </Link>
        <a
          className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700"
          href="/admin/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Admin
        </a>
      </div>
      <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">
        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
          Getting Started
        </div>

        <Link to="/" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
          Home
        </Link>
        <a
          className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700"
          href="/admin/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Admin
        </a>
      </div>
      <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">
        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
          Getting Started
        </div>

        <Link to="/" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
          Home
        </Link>
        <a
          className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700"
          href="/admin/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Admin
        </a>
      </div>
      <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">
        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
          Getting Started
        </div>

        <Link to="/" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
          Home
        </Link>
        <a
          className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700"
          href="/admin/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Admin
        </a>
      </div>

    </div>

    <div className="pt-2">
      <div className="flex pb-5 px-3 m-auto pt-5
            border-t border-gray-500 text-gray-400 text-sm
            flex-col md:flex-row max-w-6xl"
      >
        <div className="mt-2">
          © Copyright 1998-year. All Rights Reserved.
        </div>

        <div className="md:flex-auto md:flex-row-reverse mt-2 flex-row flex">
          <a href="#" className="w-6 mx-1">
            <i className="uil uil-facebook-f" />
          </a>
          <a href="#" className="w-6 mx-1">
            <i className="uil uil-twitter-alt" />
          </a>
          <a href="#" className="w-6 mx-1">
            <i className="uil uil-youtube" />
          </a>
          <a href="#" className="w-6 mx-1">
            <i className="uil uil-linkedin" />
          </a>
          <a href="#" className="w-6 mx-1">
            <i className="uil uil-instagram" />
          </a>
        </div>
      </div>
    </div>
  </footer>
);

export default FooterTemplate;
