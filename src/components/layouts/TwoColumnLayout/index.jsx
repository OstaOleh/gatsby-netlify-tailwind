import React from 'react';
import PropTypes from 'prop-types';

const TwoColumnLayout = ({ firstColumnContent, secondColumnContent }) => (
  <div className="grid grid-cols-1 gap-6 mt-4 md:grid-cols-2">
    <div className="flex items-center justify-center w-full h-32 bg-white rounded-md shadow-md">
      {firstColumnContent}
    </div>
    <div className="flex items-center justify-center w-full h-32 bg-white rounded-md shadow-md">
      {secondColumnContent}
    </div>
  </div>
);

TwoColumnLayout.propTypes = {
  firstColumnContent: PropTypes.element.isRequired,
  secondColumnContent: PropTypes.element.isRequired,
};

export default TwoColumnLayout;
