/* eslint-disable max-len */
import React from 'react';

const soundcloud = `
    <iframe 
        width="100%" 
        height="166" 
        scrolling="no" 
        frameborder="no" 
        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/379775672&color=%23ff5500&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"
    >
    </iframe>`;

const SoundCloudWidget = () => (
  <div
    // eslint-disable-next-line react/no-danger
    dangerouslySetInnerHTML={{ __html: soundcloud }}
  />
);

export default SoundCloudWidget;
